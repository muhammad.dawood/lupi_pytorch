# -*- coding: utf-8 -*-
"""
Created on Mon Apr 23 12:35:14 2018

@author: Wajid Abbasi
"""


import theano.tensor as T
from sklearn.model_selection import LeaveOneOut
from sklearn import cross_validation
from keras import backend as K
import numpy as np
import theano,os
import pandas
from keras.models import Sequential
from keras.layers import Dense, Activation,Input,Dropout
from keras.wrappers.scikit_learn import KerasRegressor,KerasClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
import scipy.stats
from keras import backend as K
from keras.utils import to_categorical
from keras.objectives import categorical_crossentropy 
from sklearn.metrics import mean_squared_error,roc_auc_score,average_precision_score
from keras.regularizers import l2
#from roc import *


def load_affinity_values(info_path, column):
    "This method return experimental affinity values as dictionary with complex ID as key and affinity as value"
    affinity_data_dic={}
    affinity_data=np.loadtxt(info_path,dtype='str',delimiter='\t')#[0:]
    affinity_data=affinity_data[1:]
    for i in range(len(affinity_data)):
        if affinity_data[i][0][:4] in affinity_data_dic.keys():
            print (affinity_data[i][0][:4])
        affinity_data_dic[affinity_data[i][0][:4]]=[float(affinity_data[i][column]),affinity_data[i][1]]
    return affinity_data_dic
def mean_varrianace_normalization(examples,std='T'):
    from sklearn import preprocessing
    #np.save('2mer_ungoup_feats_mean.npy',np.mean(examples,axis=0))
    #np.save('2mer_ungoup_feats_std.npy',((np.std(examples,axis=0))+1e-9))
    if std=='T':
        examples=preprocessing.scale(examples)
    examples=preprocessing.normalize(examples)
    return examples
def generate_standardized_complex_level_examples(feat_path,affinity_info,normalize='T'):
    values=[]
    complex_id=[]
    examples=[]
    group=[]
    #affinity_info=load_affinity_values('/media/wajid/PhD_Projects/Projects/Affinity_prediction_project/dataset/yugandhar_info.txt', affinity_col)
    for key in affinity_info:
        if os.path.exists(feat_path+key+'.npy') and os.path.exists('features/complex_level/Dias_etal_2017/'+key+'.npy')and os.path.exists('features/complex_level/moal_2011/'+key+'.npy'):
            examples.append(np.load(feat_path+key+'.npy'))
            values.append(affinity_info[key][0])
            complex_id.append(key)
            group.append(affinity_info[key][1])
        else:
            print (key)
    if normalize=='T':
        examples=mean_varrianace_normalization(np.asarray(examples))
    return np.asarray(examples),np.asarray(values),np.asarray(complex_id),np.asarray(group)
def generate_standardized_examples(feat_path,affinity_info,normalize='T'):
    values=[]
    complex_id=[]
    ligand=[]
    receptor=[]
    group=[]
    #affinity_info=load_affinity_values('/media/wajid/PhD_Projects/Projects/Affinity_prediction_project/LUPI/dataset_for_clf/yugandhar_info.txt', affinity_col)
    for key in affinity_info:
        if os.path.exists(feat_path+key+'_l_u.npy') and os.path.exists(feat_path+key+'_r_u.npy') and os.path.exists('features/complex_level/Dias_etal_2017/'+key+'.npy')and os.path.exists('features/complex_level/moal_2011/'+key+'.npy'):
            ligand.append(np.load(feat_path+key+'_l_u.npy'))
            receptor.append(np.load(feat_path+key+'_r_u.npy'))
            values.append(affinity_info[key][0])
            complex_id.append(key)
            group.append(affinity_info[key][1])
    if normalize=='T':    
        #ligand=mean_varrianace_normalization(np.asarray(ligand),std='F')
        #receptor==mean_varrianace_normalization(np.asarray(receptor),std='F')
        examples=[np.concatenate((ligand[i],receptor[i]),axis=0) for i in range(len(ligand))]
        examples=mean_varrianace_normalization(np.asarray(examples))
 
    else:
        examples=[np.concatenate((ligand[i],receptor[i]),axis=0) for i in range(len(ligand))]
        #examples=mean_varrianace_normalization(np.asarray(examples))
    return np.asarray(examples),np.asarray(values),np.asarray(complex_id),np.asarray(group)
def softmax(w, t = 1.0):
    e = np.exp(w / t)
    return e/np.sum(e,1)[:,np.newaxis]
def pearson_cor(y_true, y_pred):
    fsp = y_pred - K.mean(y_pred,axis=0) #you take the mean over the batch, keeping the features separate.   
    fst = y_true - K.mean(y_true,axis=0) 
    #mean shape: (1,10)
    #fst shape keeps (batch,10)

    devP = K.std(y_pred,axis=0)  
    devt = K.std(y_true,axis=0)
    #dev shape: (1,10)

    return K.sum(K.mean(fsp*fst,axis=0)/(devP*devt))
    
def weighted_loss(base_loss,l):
    def loss_function(y_true, y_pred):
        return l*base_loss(y_true,y_pred)
    return loss_function
    
def root_mean_squared_error(y_true, y_pred):
        return K.sqrt(K.mean(K.square(y_pred - y_true), axis=-1)) 
#==============================================================================
# def MLP(input_shape):
#     model = Sequential()
#     model.add(Dense(int(input_shape/2),input_dim=int(input_shape),kernel_initializer='normal',activation='relu'))
#     model.add(Dense(2,kernel_initializer='normal',activation='softmax'))
#     model.compile(optimizer='rmsprop',  loss='binary_crossentropy', metrics=['accuracy'])
#     return model
#==============================================================================
def MLP(input_shape):
    model = Sequential()
    model.add(Dense(int(input_shape), input_dim=int(input_shape),W_regularizer=l2(0.1),kernel_initializer='normal'))
    #model.add(Activation('relu'))
    #model.add(Dense(int(input_shape), input_dim=int(input_shape),W_regularizer=l2(0.01),kernel_initializer='normal'))
    #model.add(Activation('tanh'))
    #model.add(Dense(int(input_shape),W_regularizer=l2(0.01),kernel_initializer='normal'))
    #model.add(Activation('tanh'))
    model.add(Dense(2))    
    model.add(Activation('softmax'))
    model.compile('Adam','hinge',metrics=['accuracy'])
    return model
def distillation(input_shape):
    from keras.models import Model
    inputs = Input(shape=(input_shape,), name = 'x')
    w1 = Dense(int(input_shape),kernel_initializer='normal',activation='relu')(inputs)
    w2 = Dense(int(input_shape),kernel_initializer='normal',activation='relu')(w1)
    #w3 = Dense(int(input_shape/2),kernel_initializer='normal',activation='relu')(w2)

    w4 = Dense(2,kernel_initializer='normal')(w2)
    hard_softmax = Activation('softmax', name = 'hard')(w4)
    soft_softmax = Activation('softmax', name = 'soft')(w4)
  
#    graph = Graph()
#    graph.add_input(name='x', input_shape=(d,))
#    graph.add_node(Dense(q), name='w3', input='x')
#    graph.add_node(Activation('softmax'), name='hard_softmax', input='w3')
#    graph.add_node(Activation('softmax'), name='soft_softmax', input='w3')
#    graph.add_output(name='hard', input='hard_softmax')
#    graph.add_output(name='soft', input='soft_softmax')
    model = Model(inputs=inputs, outputs=(hard_softmax,soft_softmax))
    
    loss_hard = weighted_loss(categorical_crossentropy,1.-l)
    loss_soft = weighted_loss(categorical_crossentropy,t*t*l)
    model.compile(optimizer = 'adam', loss = {'hard':loss_hard, 'soft':loss_soft},metrics=['accuracy'])
#    graph.compile('rmsprop', {'hard':loss_hard, 'soft':loss_soft})
    return model    
  
    
#    
#def baseline_model():
#    model=Sequential()
#    model.add(Dense(int(P_input_shape/10), input_dim=P_input_shape, kernel_initializer='normal', activation='relu'))
#    model.add(Dense(int(P_input_shape/20),kernel_initializer='normal',activation='relu'))
#    #model.add(Dense(input_shape/30,kernel_initializer='normal',activation='relu'))
#    model.add(Dense(1,kernel_initializer='normal'))
#    model.compile(loss=root_mean_squared_error, optimizer='adam',metrics=[root_mean_squared_error])
#    return model
 
def train_test_reg(examples,aff,complex_id,group,regressor,epochs=100, batch_size=10, verbose=1):
    predicted_score=[]
    actual_score=[]
    loo = cross_validation.LeaveOneOut(examples.shape[0])
    if os.path.exists('out_file.txt'):
        os.remove('out_file.txt')
    for train_i, test_i in loo:
        from copy import deepcopy
        reg=deepcopy(regressor)
        reg.fit(examples[train_i], aff[train_i],epochs=epochs, batch_size=batch_size, verbose=verbose)
        #print svr.best_estimator_
        print(complex_id[test_i],group[test_i],aff[test_i],reg.predict(examples[test_i]))
        predicted_score.extend(reg.predict(examples[test_i])[0])
        #print reg.predict_proba(examples[test_i])
        #predicted_score.append(reg.predict_proba(examples[test_i])[0][1])#for random forest
        actual_score.append(aff[test_i][0])
        #with open('out_file.txt', 'ab') as f:
           # writer = csv.writer(f)
            #writer.writerow([complex_id[test_i][0],group[test_i],values[test_i][0],reg.predict(examples[test_i])[0]])
    #print "AUC-ROC:",roc_auc_score(values,predicted_score)
    #print "AUC-PR:",average_precision_score(values,predicted_score)
    #print "AUC-ROC-10:",roc(predicted_score,np.asarray(values),rocN=0.1)[2]
    #with open('test1.txt',"wb") as f:
        #np.savetxt(f, np.c_[complex_id,group,actual_score,predicted_score],fmt='%s')
    print ("Pearson Correlation Coefficient is:",stats.pearsonr(predicted_score,actual_score))
    print ("Spearman Correlation Coefficient is:",stats.spearmanr(predicted_score,actual_score))
    print ("RMSE:",np.sqrt(np.mean((np.asarray(predicted_score)-np.asarray(actual_score))**2)))
    return predicted_score,actual_score 
def train_test_clf(examples,labels,aff,complex_id,group,regressor,epochs=100, batch_size=10, verbose=1,soft_labels=None,dist='no',act_labels=None):
    predicted_score=[]
    actual_score=[]
    loo = cross_validation.LeaveOneOut(examples.shape[0])
    if os.path.exists('out_file.txt'):
        os.remove('out_file.txt')
    for train_i, test_i in loo:
        from copy import deepcopy
        
        if dist=='yes':
            reg=distillation(int(examples.shape[1]))
            reg.fit(examples[train_i], {'hard':labels[train_i], 'soft':soft_labels[train_i]},epochs=epochs, batch_size=batch_size, verbose=verbose)
        else: 
            reg=MLP(int(examples.shape[1]))
            reg.fit(examples[train_i], labels[train_i],epochs=epochs, batch_size=batch_size, verbose=verbose)
        #print svr.best_estimator_
        print(complex_id[test_i],group[test_i],aff[test_i],reg.predict(examples[test_i])[0])
        if dist=='yes':
            predicted_score.append(reg.predict(examples[test_i])[0][0][1])
        else:        
            predicted_score.append(reg.predict(examples[test_i])[0][1])
        #print reg.predict_proba(examples[test_i])
        #predicted_score.append(reg.predict_proba(examples[test_i])[0][1])#for random forest
        actual_score.append(aff[test_i][0])
        #with open('out_file.txt', 'ab') as f:
           # writer = csv.writer(f)
            #writer.writerow([complex_id[test_i][0],group[test_i],values[test_i][0],reg.predict(examples[test_i])[0]])
    print ("AUC-ROC:",roc_auc_score(act_labels,predicted_score))
    print ("AUC-PR:",average_precision_score(act_labels,predicted_score))
    #print ("AUC-ROC-10:",roc(predicted_score,np.asarray(act_labels),rocN=0.1)[2])
    #with open('test1.txt',"wb") as f:
        #np.savetxt(f, np.c_[complex_id,group,actual_score,predicted_score],fmt='%s')
#    print ("Pearson Correlation Coefficient is:",stats.pearsonr(predicted_score,actual_score))
#    print ("Spearman Correlation Coefficient is:",stats.spearmanr(predicted_score,actual_score))
    print ("RMSE:",np.sqrt(np.mean((np.asarray(predicted_score)-np.asarray(actual_score))**2)))
    return predicted_score,actual_score 
class TensorFlowTheanoFunction(object): 
    
  def __init__(self, inputs, outputs):
    self._inputs = inputs
    self._outputs = outputs

  def __call__(self, *args, **kwargs):
    import tensorflow as tf
    sess = tf.InteractiveSession()
    feeds = {}
    for (argpos, arg) in enumerate(args):
      feeds[self._inputs[argpos]] = arg
    return tf.get_default_session().run(self._outputs, feeds)
def main():
    affinity_info=load_affinity_values('yugandhar_info.txt', 2)
    previlleged_feats,aff_values,complex_id,group=generate_standardized_complex_level_examples('./features/complex_level/moal_2011/',affinity_info,normalize='T')
    previlleged_feats=previlleged_feats[:,1:]
    
    feats,aff_values,complex_id,group=generate_standardized_examples('features/2-mer/',affinity_info,normalize='T')    
    act_labels=np.asarray([0]*len(aff_values))
    act_labels[np.where( aff_values<-10.86)]=1


    P_input_shape=int(previlleged_feats.shape[1])
    I_input_shape=int(feats.shape[1])
    labels=np.asarray([0]*len(aff_values))
    labels[np.where( aff_values<-10.86)]=1
    labels = to_categorical(labels)
    examples=previlleged_feats
   
   
    #estimator = KerasClassifier(build_fn=MLP, epochs=10, batch_size=10, verbose=1)
    #estimator.fit(previlleged_feats, labels)
    #kfold = KFold(n_splits=5)
    #results = cross_val_score(estimator, previlleged_feats, labels, cv=kfold)
    #print("Results: %.2f (%.2f) MSE" % (results.mean(), results.std()))
    print("Keras version")
    mlp_dist = distillation(int(I_input_shape))
    print("Previlleged:")
    mlp_priv=MLP(int(P_input_shape))
    train_test_clf(previlleged_feats,labels,aff_values,complex_id,group,mlp_priv,epochs=200, batch_size=10, verbose=0,act_labels=act_labels)
    print("Input:")
    mlp_input=MLP(int(I_input_shape))
    train_test_clf(feats,labels,aff_values,complex_id,group,mlp_input,epochs=100, batch_size=10, verbose=0,act_labels=act_labels)

    
    mlp_priv.fit(previlleged_feats,labels,epochs=100, batch_size=10, verbose=0)
    
    for l in mlp_priv.layers:
        l.trainable=False
    #sess = tf.InteractiveSession()
    soften = K.function([mlp_priv.layers[0].input], [mlp_priv.layers[-2].output])
    #soften=TensorFlowTheanoFunction([model1.layers[0].input], model1.layers[1].output)
    #soften = theano.function([model1.layers[0].input], model1.layers[1].output)
    print("Distilled:")
    p_tr   = softmax(soften([previlleged_feats.astype(np.float32),])[0],t)
    #p_tr   = softmax(soften(previlleged_feats.astype(np.float32)),t)
    #import pdb; pdb.set_trace()
    train_test_clf(feats,labels,aff_values,complex_id,group,mlp_dist,epochs=100, batch_size=10, verbose=0,soft_labels=p_tr,dist='yes',act_labels=act_labels)

    1/0
    #feats,aff_values,complex_id,group=generate_standardized_examples('D:/Wajid PhD/Affinity_prediction_project/features/2-mer/',affinity_info,normalize='T')    
    
    
    
    model=baseline_model()
    train_test(examples,aff_values,complex_id,group,model,epochs=100, batch_size=10, verbose=1)
    1/0
    estimator = KerasRegressor(build_fn=baseline_model, epochs=100, batch_size=10, verbose=1)
    
    
    kfold = KFold(n_splits=5)
    results = cross_val_score(estimator, feats, aff_values, cv=kfold)
    print("Results: %.2f (%.2f) MSE" % (results.mean(), results.std()))
if __name__=="__main__":
    P_input_shape=None
    I_input_shape=None
    t=5
    l=1
    main()